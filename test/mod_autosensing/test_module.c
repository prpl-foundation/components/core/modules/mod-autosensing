/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxm/amxm.h>
#if defined(SAHTRACES_ENABLED)
#include <debug/sahtrace.h>
#endif

#include "../common/common_functions.h"

#include "test_module.h"
#include "mod_autosensing.h"

#define MOD_NAME "target_module"
#define MOD_PATH "../modules_under_test/" MOD_NAME ".so"

static int sensing_started = 0;
static int sensing_stopped = 0;
amxc_var_t* next_mode = NULL;

static int autosensing_set_wan_mode(UNUSED const char* function_name,
                                    amxc_var_t* args,
                                    UNUSED amxc_var_t* ret) {
    int result = 0;
    sensing_started++; // The set mode will also start the sensing

    assert_non_null(args);
    assert_int_equal(amxc_var_compare(next_mode, args, &result), 0);
    assert_int_equal(result, 0);
    return 0;
}

static int autosensing_is_up_query_start(UNUSED const char* function_name,
                                         UNUSED amxc_var_t* args,
                                         UNUSED amxc_var_t* ret) {
    sensing_started++;
    return 0;
}

static int autosensing_is_up_query_stop(UNUSED const char* function_name,
                                        UNUSED amxc_var_t* args,
                                        UNUSED amxc_var_t* ret) {
    sensing_stopped++;
    return 0;
}

static int register_dummy_core_functions(void) {
    amxm_shared_object_t* so = amxm_get_so("self");
    amxm_module_t* mod = NULL;
    assert_non_null(so);
    assert_int_equal(amxm_module_register(&mod, so, MOD_DM_MNGR), 0);
    assert_int_equal(amxm_module_add_function(mod, "set-mode", autosensing_set_wan_mode), 0);
    assert_int_equal(amxm_module_add_function(mod, "isup-sensing-start", autosensing_is_up_query_start), 0);
    assert_int_equal(amxm_module_add_function(mod, "isup-sensing-stop", autosensing_is_up_query_stop), 0);

    return 0;
}

int test_setup(UNUSED void** state) {
    register_dummy_core_functions();
#if defined(SAHTRACES_ENABLED)
    sahTraceOpen("unittest", 0);
    sahTraceSetLevel(500);
    sahTraceAddZone(500, "mod-autosensing");
#endif
    return 0;
}

int test_teardown(UNUSED void** state) {
    return 0;
}

void test_can_load_module(UNUSED void** state) {
    amxm_shared_object_t* so = NULL;
    assert_int_equal(amxm_so_open(&so, MOD_NAME, MOD_PATH), 0);
}

void test_has_functions(UNUSED void** state) {
    assert_true(amxm_has_function(MOD_NAME, MOD_AUTOSENSING_CTRL, "autosensing-start"));
    assert_true(amxm_has_function(MOD_NAME, MOD_AUTOSENSING_CTRL, "autosensing-stop"));
}

/*
 * This tests simulates atboot autosensing of thee modes, starting with the current
 * Expectations:
 *      * Sensing is configured for at boot so every mode should only be sensed once
 *      * There is a current mode that needs to be sensed first (a timeout should happen before we expect a mode to be set)
 *      * There are three modes that should be sensed
 */
void test_sensing_atboot(UNUSED void** state) {
    amxc_var_t* data = NULL;
    amxc_var_t ret;
    amxc_var_t* modes = NULL;
    int i = 0;

    sensing_stopped = 0;
    sensing_started = 0;
    amxc_var_init(&ret);

    data = read_json_from_file("test_data/test_atboot.json");
    modes = GET_ARG(data, "modes");
    amxm_execute_function(MOD_NAME, MOD_AUTOSENSING_CTRL, "autosensing-start", data, &ret);

    amxc_var_for_each(mode, modes) {
        next_mode = mode;
        i++;
        assert_int_equal(sensing_stopped, i);
        assert_int_equal(sensing_started, i);
        assert_true(read_sig_alarm());
    }

    next_mode = NULL;
    assert_int_equal(sensing_stopped, 4); // Initial mode + 3 modes
    assert_int_equal(sensing_started, 4); // Initial mode + 3 modes
    assert_true(read_sig_alarm());        // Catch the last mode timeout

    // After all modes are sensed, sensing should stop so timer should run out
    assert_false(read_sig_alarm());

    amxm_execute_function(MOD_NAME, MOD_AUTOSENSING_CTRL, "autosensing-stop", data, &ret);

    amxc_var_clean(&ret);
    amxc_var_delete(&data);
}

/*
 * This tests simulates continuous autosensing of thee modes, starting with the current
 * Expectations:
 *      * Sensing is configured for continuous so after the last mode, the first mode should be selected again
 *      * There is a current mode that needs to be sensed first (a timeout should happen before we expect a mode to be set)
 *      * There are three modes that should be continuously sensed
 */
void test_sensing_continuous(UNUSED void** state) {
    amxc_var_t* data = NULL;
    amxc_var_t ret;
    amxc_var_t* modes = NULL;
    int i = 0;

    sensing_stopped = 0;
    sensing_started = 0;
    amxc_var_init(&ret);

    data = read_json_from_file("test_data/test_continuous.json");
    modes = GET_ARG(data, "modes");
    amxm_execute_function(MOD_NAME, MOD_AUTOSENSING_CTRL, "autosensing-start", data, &ret);

    amxc_var_for_each(mode, modes) {
        next_mode = mode;
        i++;
        assert_int_equal(sensing_stopped, i);
        assert_int_equal(sensing_started, i);
        assert_true(read_sig_alarm());
    }

    next_mode = GETI_ARG(modes, 0);
    i++;
    assert_int_equal(sensing_stopped, 4); // Initial mode + 3 modes
    assert_int_equal(sensing_started, 4); // Initial mode + 3 modes
    assert_true(read_sig_alarm());        // Catches the last mode timeout, will try to set the first mode again

    next_mode = GETI_ARG(modes, 1);
    assert_int_equal(sensing_stopped, 5); // Initial mode + 3 modes + 1ste mode again
    assert_int_equal(sensing_started, 5); // Initial mode + 3 modes + 1ste mode again
    assert_true(read_sig_alarm());        // Catches the first mode again, will try to set the second mode

    // Sensing will go on until active mode is found and the stop is called
    amxm_execute_function(MOD_NAME, MOD_AUTOSENSING_CTRL, "autosensing-stop", data, &ret);

    // After stopping the sensing, no timer should run out
    assert_false(read_sig_alarm());

    amxc_var_clean(&ret);
    amxc_var_delete(&data);
}

/*
 * This tests simulates atboot autosensing of thee modes, the current mode should not be sensed
 * Expectations:
 *      * Sensing is configured for at boot but we simulate finding a mode
 *      * There is NO current mode that needs to be sensed first, a new mode should be set immediately
 *      * There are three modes that can be sensed but we stop after setting the second mode, simulating a mode being found
 */
void test_sensing_atboot_no_current(UNUSED void** state) {
    amxc_var_t* data = NULL;
    amxc_var_t ret;
    amxc_var_t* modes = NULL;
    int i = 0;

    sensing_stopped = 0;
    sensing_started = 0;
    amxc_var_init(&ret);

    data = read_json_from_file("test_data/test_atboot_no_current.json");
    modes = GET_ARG(data, "modes");
    // We expect the a new mode will be set directly after calling the start
    next_mode = GETI_ARG(modes, 0);
    i++;
    amxm_execute_function(MOD_NAME, MOD_AUTOSENSING_CTRL, "autosensing-start", data, &ret);
    assert_int_equal(sensing_stopped, i);
    assert_int_equal(sensing_started, i);

    next_mode = amxc_var_get_next(next_mode);
    i++;
    assert_true(read_sig_alarm());
    assert_int_equal(sensing_stopped, i);
    assert_int_equal(sensing_started, i);

    next_mode = NULL;
    amxm_execute_function(MOD_NAME, MOD_AUTOSENSING_CTRL, "autosensing-stop", data, &ret);
    assert_int_equal(sensing_stopped, 2); // Initial stop and first mode
    assert_int_equal(sensing_started, 2); // 2 modes that where sensed before stop was called
    // After stopping the sensing, no timer should run out
    assert_false(read_sig_alarm());

    amxc_var_clean(&ret);
    amxc_var_delete(&data);
}

/*
 * This tests simulates continuous autosensing of thee modes, the current mode should not be sensed
 * Expectations:
 *      * Sensing is configured for continuous but we simulate finding a mode
 *      * There is NO current mode that needs to be sensed first, a new mode should be set immediately
 *      * There are three modes that can be sensed but we stop after setting the second mode, simulating a mode being found
 */
void test_sensing_continuous_no_current(UNUSED void** state) {
    amxc_var_t* data = NULL;
    amxc_var_t ret;
    amxc_var_t* modes = NULL;
    int i = 0;

    sensing_stopped = 0;
    sensing_started = 0;
    amxc_var_init(&ret);

    data = read_json_from_file("test_data/test_continuous_no_current.json");
    modes = GET_ARG(data, "modes");
    // We expect the a new mode will be set directly after calling the start
    next_mode = GETI_ARG(modes, 0);
    i++;
    amxm_execute_function(MOD_NAME, MOD_AUTOSENSING_CTRL, "autosensing-start", data, &ret);
    assert_int_equal(sensing_stopped, i);
    assert_int_equal(sensing_started, i);

    next_mode = amxc_var_get_next(next_mode);
    i++;
    assert_true(read_sig_alarm());
    assert_int_equal(sensing_stopped, i);
    assert_int_equal(sensing_started, i);

    next_mode = NULL;
    amxm_execute_function(MOD_NAME, MOD_AUTOSENSING_CTRL, "autosensing-stop", data, &ret);
    assert_int_equal(sensing_stopped, 2); // Initial stop and first mode
    assert_int_equal(sensing_started, 2); // 2 modes that where sensed before stop was called
    // After stopping the sensing, no timer should run out
    assert_false(read_sig_alarm());

    amxc_var_clean(&ret);
    amxc_var_delete(&data);
}

void test_can_close_module(UNUSED void** state) {
    assert_int_equal(amxm_close_all(), 0);
}
