MACHINE = $(shell $(CC) -dumpmachine)

SRCDIR = $(realpath ../../src)
OBJDIR = $(realpath ../../output/$(MACHINE)/coverage)
INCDIR = $(realpath ../../include ../../include_priv ../include)
MOCK_SRCDIR = $(realpath ../common/)

SOURCES_MODULE = $(wildcard $(SRCDIR)/*.c)
OBJECTS_MODULE = $(addprefix $(OBJDIR)/,$(notdir $(SOURCES_MODULE:.c=.o)))

SOURCES_RUN_TEST = $(wildcard $(MOCK_SRCDIR)/*.c)

CFLAGS += -Werror -Wall -Wextra -Wno-attributes\
		  --std=gnu99 -fPIC -g3 -Wmissing-declarations \
		  $(addprefix -I ,$(INCDIR)) -I$(OBJDIR)/.. \
		  -fkeep-inline-functions -fkeep-static-functions \
		   -Wno-format-nonliteral \
		  $(shell pkg-config --cflags cmocka) -pthread -DUNIT_TEST

LDFLAGS += -fkeep-inline-functions -fkeep-static-functions \
		   $(shell pkg-config --libs cmocka) \
		   -lamxc -lamxj -lamxm -lamxo -lamxp -lsahtrace

# Set to 1 enable SaH traces
ENABLE_TRACES := 0
ifeq ($(ENABLE_TRACES),1)
CFLAGS += -DSAHTRACES_ENABLED -DSAHTRACES_LEVEL=500
endif

