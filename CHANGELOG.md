# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v0.2.2 - 2024-11-25(10:48:10 +0000)

### Other

- unit tests: fix minor issues and improve

## Release v0.2.1 - 2023-05-10(08:01:56 +0000)

### Other

- [Coverage] Remove SAHTRACE defines in order to increase branching coverage

## Release v0.2.0 - 2023-02-16(16:58:45 +0000)

### New

- Implement a replacement for the LastWANMode from autosensing

