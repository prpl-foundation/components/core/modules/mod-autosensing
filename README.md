# mod_autosensing

The autosensing module is used by the wan-manager to loop over different modes in an attempt to find a mode which has connectivity.

## loading/unloading the module
The constructor and destructor functions from this module should be called to make sure that the module works and that the data is clean at the end.

## Module functions
This module provides two function that can be called by the core component.

### autosensing-start
This module will configure and start the sensing process. Depending on the data provided, the sensing behavior will be different.

#### Required data
This function expects the data variant that contains the following parameters:
* `SensingPolicy`: AtBoot or Continuous, stop sensing if every mode is tried an no active mode is found or continue by looping back to the first mode
* `SensingTimeout`: Max time in seconds to try every mode before the next mode should be selected.
* `sense_current_mode`: If set to true, if will try the mode that is currently set to see if it is active, otherwise the first mode is set.
* `modes`: This is a list of variants that contain the alias of candidate modes.

An example of the data that should be provided:
```
{
    SensingPolicy = "AtBoot",
    SensingTimeout = 10,
    sense_current_mode = true,
    modes = [
        {
            Alias = "demo_wanmode"
        },
        {
            Alias = "demo_vlanmode"
        },
        {
            Alias = "demo_pppmode"
        },
        {
            Alias = "Ethernet_DHCP"
        },
        {
            Alias = "Ethernet_PPP"
        }
    ]
}
```
It is allowed to provide additional data in de data variant, but it will be ignored by the function.

### autosensing-stop
This module will stop the sensing process.
This function should be called by the core component if a valid mode is found or if sensing should no longer continue.

#### Required data
This function does not expect any data to be provided by the core component.


## Core component functions
The core component using this module, should have three module functions registered that can be called by the module.
### set-mode
The module will call this function and provide it with the next variant in the modes list provided in the autosensing-start function.
The function should configure this mode and start the sensing on this mode.
No return data is expected

#### Example data
From the above sample data, the module could call this function with data like this:
```
    {
        Alias = "demo_vlanmode"
    }
```

### isup-sensing-start
The module will call this function if it wants to start the sensing on the current mode.
This is done when the current mode needs to be sensed.
No data will be provided to this function and no return is expected.

### isup-sensing-stop
The module will call this function if it wants to stop the sensing of the current mode.
No data will be provided to this function and no return is expected.
