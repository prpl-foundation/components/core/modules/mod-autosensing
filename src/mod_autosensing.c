/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <string.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxm/amxm.h>

#include "mod_autosensing.h"

static int autosensing_stop(const char* function_name, amxc_var_t* args, amxc_var_t* ret);

typedef struct {
    amxc_var_t* modes;
    amxc_var_t* next_mode;
    amxp_timer_t* timer;
    uint32_t timeout;
    bool continuous;
    bool in_progress;
} auto_sensing_t;

static auto_sensing_t auto_sensing = {
    .modes = NULL,
    .next_mode = NULL,
    .timer = NULL,
    .timeout = 10000,
    .continuous = false,
    .in_progress = false
};

static void auto_sensing_timer_cb(UNUSED amxp_timer_t* const timer, UNUSED void* data) {
    int rv = -1;
    amxc_var_t args;
    amxc_var_t ret;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    when_false_trace(auto_sensing.in_progress, exit, WARNING, "Autosensing no longer in progress");

    rv = amxm_execute_function("self", MOD_DM_MNGR, "isup-sensing-stop", &args, &ret);
    if(rv != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to stop sensing, '%d'", rv);
    }

    SAH_TRACEZ_INFO(ME, "Verifying next wanmode");
    if(auto_sensing.next_mode == NULL) {
        SAH_TRACEZ_INFO(ME, "All modes where sensed, no mode was active");
        if(auto_sensing.continuous == true) {
            SAH_TRACEZ_INFO(ME, "Selecting the first wan-mode again");
            auto_sensing.next_mode = GETI_ARG(auto_sensing.modes, 0);
        } else {
            SAH_TRACEZ_INFO(ME, "Stopping sensing");
            auto_sensing.in_progress = false;
            goto exit;
        }
    }

    amxp_timer_start(timer, auto_sensing.timeout);
    rv = amxm_execute_function("self", MOD_DM_MNGR, "set-mode", auto_sensing.next_mode, &ret);
    if(rv != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to call set-mode function, '%d'", rv);
        autosensing_stop(NULL, &args, &ret);
        goto exit;
    }
    auto_sensing.next_mode = amxc_var_get_next(auto_sensing.next_mode);

exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

static int autosensing_start(UNUSED const char* function_name,
                             amxc_var_t* args,
                             UNUSED amxc_var_t* ret) {
    int rv = -1;

    SAH_TRACEZ_INFO(ME, "Starting auto-sensing");

    when_false_trace(!auto_sensing.in_progress, exit, WARNING, "Sensing already in progress, not restarting");

    auto_sensing.next_mode = NULL;
    amxc_var_delete(&auto_sensing.modes);
    amxc_var_new(&auto_sensing.modes);

    rv = amxc_var_copy(auto_sensing.modes, GET_ARG(args, "modes"));
    when_failed_trace(rv, exit, ERROR, "Failed to copy modes to be sensed");

    amxp_timer_stop(auto_sensing.timer); // Make sure the timer is not running
    auto_sensing.timeout = GET_UINT32(args, "SensingTimeout") * 1000;
    const char* policy = GET_CHAR(args, "SensingPolicy");
    auto_sensing.continuous = (policy != NULL) && (strcmp(policy, "Continuous") == 0);

    // Set sensing status to in progress
    auto_sensing.in_progress = true;

    // Timer should be started before calling the set mode.
    // If this mode is already set and valid, sensing will be stopped immediately
    // the timer will then be started after sensing is stopped
    amxp_timer_start(auto_sensing.timer, auto_sensing.timeout);

    // Select the next wan mode
    auto_sensing.next_mode = GETI_ARG(auto_sensing.modes, 0);
    // Make sure there are no queries on the current mode
    amxm_execute_function("self", MOD_DM_MNGR, "isup-sensing-stop", args, ret);
    if(!GET_BOOL(args, "sense_current_mode")) {
        SAH_TRACEZ_INFO(ME, "Current WANMode should not be sensed, selecting first mode");
        rv = amxm_execute_function("self", MOD_DM_MNGR, "set-mode", auto_sensing.next_mode, ret);
        when_failed_trace(rv, exit, ERROR, "Failed to call set-mode for mode '%s', returned '%d'", GET_CHAR(auto_sensing.next_mode, "Alias"), rv);
        auto_sensing.next_mode = amxc_var_get_next(auto_sensing.next_mode);
    } else {
        SAH_TRACEZ_INFO(ME, "Sensing current mode first");
        // Start a query on the current mode
        rv = amxm_execute_function("self", MOD_DM_MNGR, "isup-sensing-start", args, ret);
    }

exit:
    return rv;
}

static int autosensing_stop(UNUSED const char* function_name,
                            UNUSED amxc_var_t* args,
                            UNUSED amxc_var_t* ret) {
    int rv = amxp_timer_stop(auto_sensing.timer);

    SAH_TRACEZ_INFO(ME, "Stopping auto-sensing");

    auto_sensing.in_progress = false;
    amxc_var_delete(&auto_sensing.modes);
    when_failed_trace(rv, exit, ERROR, "Failed to stop timer");

exit:
    return rv;
}

static AMXM_CONSTRUCTOR mod_autosensing_start(void) {
    amxm_shared_object_t* so = amxm_so_get_current();
    amxm_module_t* mod = NULL;

    amxm_module_register(&mod, so, MOD_AUTOSENSING_CTRL);

    amxm_module_add_function(mod, "autosensing-start", autosensing_start);
    amxm_module_add_function(mod, "autosensing-stop", autosensing_stop);

    amxp_timer_new(&auto_sensing.timer, auto_sensing_timer_cb, NULL);

    return 0;
}

static AMXM_DESTRUCTOR mod_autosensing_stop(void) {
    auto_sensing.in_progress = false;
    auto_sensing.next_mode = NULL;
    amxp_timer_delete(&auto_sensing.timer);
    amxc_var_delete(&auto_sensing.modes);
    return 0;
}
